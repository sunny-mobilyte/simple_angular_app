// ...and add 'ui.router' as a dependency
var myApp = angular.module('myApp', ['ui.router']);

myApp.config(['$httpProvider', '$stateProvider', '$urlRouterProvider', function($httpProvider, $stateProvider, $urlRouterProvider) {
     $urlRouterProvider.otherwise('/list');
     $stateProvider
     .state('list', {
           url: '/list',
           controller : 'ColorController',
           templateUrl: 'list.html',
      });
}]);
