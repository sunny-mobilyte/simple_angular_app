module.exports = function($httpProvider, $stateProvider, $urlRouterProvider) {
     $urlRouterProvider.otherwise('/list');
     $stateProvider
     .state('list', {
           url: '/list',
           controller : 'ColorController',
           templateUrl: 'list.html'
     });
}
