myApp.service('ColorService', ['$http','$rootScope', function ($http, $rootScope) {
     return {
          list : function (callback) {
               $http.get('/colors/list/').then(
                    function (err) {
                         callback(err);
                    },
                    function (data) {
                         callback(data);
                    }
               )
          },

     }
}]);
