myApp.controller('ColorController', ['$scope','$rootScope','$state','$stateParams','ColorService', function ($scope,$rootScope,$state, $stateParams, ColorService) {


     var getColors =  function () {
          ColorService.list(function (response) {
               $scope.colors = response.data.data;
          });
      }();

}]);
