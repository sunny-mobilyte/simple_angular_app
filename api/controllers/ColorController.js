module.exports = {

     list: function(req, res) {
          var request = require('request');
          var url = "http://www.colourlovers.com/api/palettes";
          request(url, function(error, response, html){
               if(error){
                    return res.json ({
                         status : "success",
                         message : "Some error occured, while retrieving data."
                    });
               } else {
                    var parser = require('xml2json');
                    var json = JSON.parse(parser.toJson(response.body)).palettes.palette;
                    return res.json ({
                         status : "success",
                         message : "Colors Data.",
                         data : json
                    });
               }
          }) ;
     }
}
