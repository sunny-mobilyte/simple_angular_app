module.exports = function(app, express) {
     var router = express.Router();

     var controllerObj = require('./../api/controllers/ColorController.js');
     router.get('/list/', controllerObj.list);

     app.use('/colors', router);
}
